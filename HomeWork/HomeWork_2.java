// import java.net.*;
// import java.net.URI;
// import java.io.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


// import java.io.*;

class HomeWork_2 {

	static void start() {
		System.out.println("\nHello! Program has started.\n");
	}

	static void end() {
		System.out.println("\nProgram has ended. Goodbye!\n");
	}

	/** Task_1 */

	static void printTower (int num) {

		for (int line=1; line<=num; line++){

			String hashtags = "##";
			String spaces= "";

			for (int i=1; i<line; i++) {
				hashtags+="#";
			}
			for (int j=1; j<=num-line; j++) {
				spaces+=" ";
			}

			System.out.println(spaces + hashtags + "  " + hashtags + spaces);
		}
	}

  	/** Task_2 */
  	
	static String searchFromUrl_Domain(String url) {

		Pattern p = Pattern.compile("(https?://)?(www\\.)?([^:^/]*)(:\\d*)?(.*)?");
		Matcher m = p.matcher(url);

		if (m.find()) {
			return m.group(3); //domain
			// return m.group(1); //protocol
			// return m.group(2); //www
			// return m.group(4); //port
			// return m.group(5); //uri
		} else {
			return "error";
		}

	}

//////////////////////////////////////////////////////
	public static void main(String[] args) {
		start();

		printTower(6);
	    System.out.println(searchFromUrl_Domain("text"));
  		
  		end();
	}
//////////////////////////////////////////////////////
}