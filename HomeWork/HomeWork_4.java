import java.util.Scanner;
import java.util.Random;
import java.util.HashMap;
import java.util.Map.Entry;

class HomeWork_4 {
// пользователь вводит данные
	static int min;
	static int max;
	static int n;

	static void enterValues() {
	    Scanner input = new Scanner(System.in);
	    System.out.println ("\nHello!");
	    System.out.println ("\nPlease enter MINimum value:");
	    min = input.nextInt();
	    System.out.println ("\nPlease enter MAXimum value:");
	    max = input.nextInt();
	    System.out.println ("\nPlease enter the amount of random generated values:");
	    n = input.nextInt();
	}
// сгенерировать случайное число
	static int generateRandom(int min, int max) {
	    Random r = new Random();
	    return r.nextInt((max - min) + 1) + min;
	}
// создать новый массив hashmap в зависимости от введёных чисел и заполнить его случайными числами. посчитать сколько раз повторялось число	
	static HashMap<Integer,Integer> createHashMap(int min, int max, int n) {
		HashMap<Integer,Integer> mapWithRandomNumbers = new HashMap<Integer,Integer>();
		for (int i=0;i<n;i++) {
			int key = generateRandom(min, max);
			mapWithRandomNumbers.put(key, mapWithRandomNumbers.containsKey(key) ? mapWithRandomNumbers.get(key) + 1 : 1);
		}
		return mapWithRandomNumbers;
	}
// вывести на экран массив hashmap
	static void printHashMap (HashMap<Integer,Integer> mapWithRandomNumbers) {
		for (Entry<Integer, Integer> entry : mapWithRandomNumbers.entrySet())
		    System.out.println("number \"" + entry.getKey() + "\" has been randomly generated \"" + entry.getValue() + "\" times");
	}
// объединить функции в одну
	static void printResult(int min, int max, int n) {
		//introduction
		System.out.println("min value = " + min + "; max value = " + max + "; number of randomly generated values is = " + n + "\n");
		printHashMap(createHashMap(min, max, n));
	}
//////////////////////////////////////////////////////////
	public static void main (String[] args) {
		enterValues();
		printResult(min, max, n);
	}
//////////////////////////////////////////////////////////
}