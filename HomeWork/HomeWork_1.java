class HomeWork_1 {

	static void start() {
		System.out.println("Hello! Program has started.");
	}

	static void end() {
		System.out.println("Program has ended. Goodbye!");
	}

	/** Task_1 */
 	static int charCount(String str) {
        int charCount=0;
        for (int i=0; i<str.length(); i++) {
            if (('e'==str.charAt(i)) || ('E'==str.charAt(i))) {
                charCount++;
            }
        }
        return charCount;
    }

    /*static int occuranceOfE(String line) { /// answer from Robert
    	return line.length() - line.replace("e", "").replace("E", "").length();
    }*/

    static void charCountPrint(String str) {
		System.out.println("Entered string is: \"" + str + "\". Character \"E\" or \"e\" is mentioned "+ charCount(str) + " times.");
    }
  
  	/** Task_2 */
	static int sum35(int x) {
		int sum=0;
		for (int i=1; i<x; i++) {
			if ((i%3==0) || (i%5==0)) {
				sum+=i;
			}
		}
		return sum;
	}

	static void sum35Print(int x) {
		System.out.println("Entered number is " + x + ". The result of function is: " + sum35(x) + ".");
	}

//////////////////////////////////////////////////////
	public static void main(String[] args) {
		start();

        charCountPrint("Else if something happensE");
		sum35Print(16);

  		end();
	}
//////////////////////////////////////////////////////
}