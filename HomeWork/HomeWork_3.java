import java.io.File;
import java.io.FileNotFoundException;

import java.util.Scanner;
import java.util.Collections;
import java.util.Comparator;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

class HomeWork_3 {

    /** 1. User enters the name of the document which should be analyzed */
    static String enterDocumentName() {
        Scanner input = new Scanner(System.in);
        System.out.println ("Hello! \nPlease enter the name of the document. \nNote: Path can be absolute or relative.");
        return input.nextLine();
    }

    /** 2. Read the document and create map with containing words */
    static HashMap<String, Integer> createVocabulary(String name) { 
        HashMap <String, Integer> vocabularyUnsorted = new HashMap <String, Integer>();
        try {
            /** 2. Program receives the document */
            System.out.println ("Document " + name + " is going to be anlayzed.");
            File document = new File(name);
            Scanner scan = new Scanner(document);
            /** 2.1. Analyze word by word in the document */
            while(scan.hasNext()) {
                String key = scan.next().replaceAll("[\\W]|_", "").toLowerCase(); /** 2.2 Clear words from non alphanumeric characters*/
                int value = 1;
                if (vocabularyUnsorted.containsKey(key)) /** 2.3 Increment repeated word */
                    value = vocabularyUnsorted.get(key) + 1; 
                vocabularyUnsorted.put(key, value);
            }
            scan.close();
        } 
        catch (FileNotFoundException e) {
                    System.out.println("The document is not found! :(");
        }
        return vocabularyUnsorted;
    }

    /** 3. Vocabulary is being sorted */
    static Map<String, Integer> sortVocabulary (Map<String, Integer> vocabularyUnsorted) {
        List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(vocabularyUnsorted.entrySet());
        /** 3.1. Sorting the list based on values */
        Collections.sort(list, new Comparator<Entry<String, Integer>>() {
            public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
                    return o2.getValue().compareTo(o1.getValue());
            }
        });
        /** 3.2 Maintaining insertion order with the help of LinkedList */
        Map<String, Integer> vocabularySorted = new LinkedHashMap<String, Integer>();
        for (Entry<String, Integer> entry : list)
            vocabularySorted.put(entry.getKey(), entry.getValue());
        return vocabularySorted;
    }

    /** 4. Show the results */
    static void printVocabulary(Map<String, Integer> vocabulary) {
        if (vocabulary.size()<1)
            System.out.println("\n  There are no words in the document.");
        else {
            System.out.println("\n The document contains such words:");
            /** 4.1. Sort existing list of words */
            int total = 0;
            for (Entry<String, Integer> entry : vocabulary.entrySet()) {
                String word = entry.getKey(); 
                int value = entry.getValue();
                System.out.println(word.substring(0,1).toUpperCase() + word.substring(1).toLowerCase() + " : " + value);
                total += value; /** 4.2. Sum values of the list */
            }
            /** 4.3. Show total results */
            System.out.println("\n As a result the document contains: " + vocabulary.size() + " words");
            System.out.println("\n Total amount of words (including repetitive) is: " + total);

        }
    }

    /** 5. Combine methods in one function */
    static void printVocabularySorted () {
        printVocabulary(
            sortVocabulary(
                createVocabulary(
                    enterDocumentName(
        ))));
    }

//////////////////////////////////////////////////////
    public static void main(String[] args) {
        printVocabularySorted();
    }
//////////////////////////////////////////////////////
}
