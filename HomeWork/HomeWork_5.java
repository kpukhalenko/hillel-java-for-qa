import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;

public class HomeWork_5 {
    private static WebDriver browser;

    @BeforeTest
    public static void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe"); //должно быть объявлено перед запуском chromedriver
        browser = new ChromeDriver();
    }

    @AfterTest
    public static void closeBrowser() throws InterruptedException {
        Thread.sleep(2000);
        browser.quit();
    }

    static WebElement clearAndFill(By selector, String data) {
        WebElement element = browser.findElement(selector);
        element.clear();
        element.sendKeys(data);
        return element;
    }

    @Test (testName = "testMainPageIsOpened", description = "First Test to check that connection to the web-site can be established")
    static void testMainPageIsOpened(){
        browser.get("https://www.linkedin.com");
        Assert.assertTrue(browser.findElement(By.cssSelector("img[alt='LinkedIn']")).isDisplayed());
    }

    @Test (testName = "testChangeLanguage", description = "User should be able to change the preferred language", dependsOnMethods = "testMainPageIsOpened")
    static void testChangeLanguage(){
        for (int i=0;i<2;i++){
            WebElement language = browser.findElement(By.cssSelector(".lang-selector-state-label"));
            language.click();
            switch (language.getText()) {
                case "Язык":
                    browser.findElement(By.cssSelector("button[data-locale='en_US']")).click();
                    Assert.assertEquals("Language", browser.findElement(By.cssSelector(".lang-selector-state-label")).getText());
                    break;
                case "Language":
                    browser.findElement(By.cssSelector("button[data-locale='ru_RU']")).click();
                    Assert.assertEquals("Язык", browser.findElement(By.cssSelector(".lang-selector-state-label")).getText());
                    break;
                default:
                    browser.findElement(By.cssSelector("button[data-locale='en_US']")).click();
                    Assert.assertEquals("Language", browser.findElement(By.cssSelector(".lang-selector-state-label")).getText());
                    break;
            }
        }
    }

    @Test (testName = "testLoginSuccessful", description = "User should successfully login", dependsOnMethods = "testChangeLanguage")
    static void testLoginSuccessful() {
        login("kpukhalenko@gmail.com", "sometestpassw0rd");
    }
    static void login(String email, String password) {
        clearAndFill(By.cssSelector(".login-email"), email);
        clearAndFill(By.cssSelector(".login-password"), password);
        browser.findElement(By.cssSelector("[class='login submit-button']")).click();
        Assert.assertTrue(browser.findElement(By.cssSelector("[class='nav-item__link nav-item__link--underline js-nav-item-link'")).isDisplayed());
    }

//    @Test (testName = "testCreateAccountExisting", description = "user should not be able to create already existing account", dependsOnMethods = "testMainPageIsOpened", priority = -1)
    static void testCreateAccountExisting() {
        createAccount("test", "test", "kpukhalenko@gmail.com", "a1234567890");
        Assert.assertTrue(browser.findElement(By.cssSelector("[class='hopscotch-bubble animated hopscotch-callout no-number']")).isDisplayed()); //pop-up with error message should be shown
    }

    static void createAccount(String firstname, String lastname, String email, String password) {
        clearAndFill(By.cssSelector(".reg-firstname"), firstname);
        clearAndFill(By.cssSelector(".reg-lastname"), lastname);
        clearAndFill(By.cssSelector(".reg-email"), email);
        clearAndFill(By.cssSelector(".reg-password"), password);
        browser.findElement(By.cssSelector("[class='registration submit-button']")).click();
    }
}
